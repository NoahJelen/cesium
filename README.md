[![Crates.io](https://img.shields.io/crates/v/cesium)](https://crates.io/crates/cesium)
[![AUR version](https://img.shields.io/aur/version/cesium-editor)](https://aur.archlinux.org/packages/cesium-editor/)
# Cesium
![](icon.png)

Cesium is a bare-bones (currently) text editor very similar to that of Windows' Notepad. It has the ability to load and save basic text files.

# Screenshots

## Main UI
![](screenshots/editor.png)

# How to install

## Arch Linux based Linux systems
Cesium, is available in the AUR for Arch Linux and any system based on it (like Manjaro Linux, EndeavourOS, and Artix Linux)

Installation example using `yay`: `yay -S cesium-editor`

## NetBSD
Thanks to a pkgsrc maintainer, Cesium is also available in NetBSD's pkgsrc repositories! It can be installed by using `pkgin install cesium`.

## Other Linux and Unix-like systems

Make sure you have the latest version of Rust installed

Instructions on how to install it are [here](https://rustup.rs/)

### Using cargo (rust's package manager)
Simply run `cargo install cesium` and that will install Cesium for you.


### Manually

After installing Rust run the following commands:

`git clone https://gitlab.com/NoahJelen/cesium`

`cd cesium`

`./build.sh `  <-- This will request root access in order to install the program

To remove: run `./remove.sh` or if install was done via cargo: `cargo uninstall cesium`

To do:
- [x] Configurable theme
- [ ] Find function
- [ ] Cut, Copy, and Paste
- [ ] Less hideous looking user interface
- [ ] GUI Support
- [ ] Vim-like command line pane
- [ ] Status bar
