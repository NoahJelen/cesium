#!/bin/sh
cargo build --release
sudo mkdir /usr/lib/cesium
sudo cp icon.png /usr/lib/cesium/icon.png
sudo cp target/release/cesium /usr/bin/cesium
sudo chmod 755 /usr/bin/cesium
sudo cp cesium.desktop /usr/share/applications/cesium.desktop

sudo install -Dt /usr/share/man/man1 man/cesium.1
sudo gzip /usr/share/man/man1/cesium.1
echo "Cesium has been installed on your system"