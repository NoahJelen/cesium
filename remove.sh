#!/bin/sh
sudo rm -rf /usr/lib/cesium
sudo rm /usr/bin/cesium
sudo rm /usr/share/applications/cesium.desktop
sudo rm /usr/share/man/man1/cesium.1.gz
echo "Cesium has been removed from your system"
