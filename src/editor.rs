use cursive::Cursive;
use cursive::views::{Dialog, TextView, TextArea};
use rust_utils::utils;
use crate::file_select;
use std::path::Path;
use chrono::{Datelike, Timelike, Utc, Local};

//clears the editor
pub fn clear_editor(view: &mut Cursive) {
    view.add_layer(
        Dialog::around(TextView::new("Are you sure?\nAll unsaved work WILL be lost!"))
        .dismiss_button("No")
        .button("Yes", |s: &mut Cursive|{
            s.call_on_name("text", |s: &mut TextArea|{
                s.set_content("");
            });
            s.call_on_name("buffer", |s: &mut Dialog|{
                s.set_title("Cesium");
            });
            s.pop_layer();
        })
    )
}

//overwrites the current file or saves a new one
pub fn overwrite(view: &mut Cursive){
    let is_new = !Path::new("/tmp/file_path").exists();
    if is_new {
        file_select::save_new_file(view);
    }
    else{
        view.call_on_name("text",|s: &mut TextArea|{
            utils::set_shared_val("buffer", s.get_content());
            let file = utils::get_shared_val("file_path");
            utils::save_file(file, s.get_content());
        });
    }
}

//inserts the date
pub fn insert_date(view: &mut Cursive) {
    let months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    let date = Utc::now().date();
    let month = months[date.month() as usize-1];
    let day = date.day();
    let year = date.year();

    let date_str = format!("{} {}, {}",month, day, year);
    view.call_on_name("text", |s: &mut TextArea|{
        utils::set_shared_val("buffer", s.get_content());
        let mut curr_cont = utils::get_shared_val("buffer");
        let loc = s.cursor();
        curr_cont.insert_str(loc, &date_str[..]);
        s.set_content(curr_cont)
    });
}

//inserts the time
pub fn insert_time(view: &mut Cursive) {
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 

    let time_str = format!("{:02}:{:02}:{:02}",hour,minute,second);
    view.call_on_name("text", |s: &mut TextArea|{
        utils::set_shared_val("buffer", s.get_content());
        let mut curr_cont = utils::get_shared_val("buffer");
        let loc = s.cursor();
        curr_cont.insert_str(loc, &time_str[..]);
        s.set_content(curr_cont)
    });
}