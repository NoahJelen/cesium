use std::env;
mod tui;
mod file_select;
mod editor;

fn main() {
    let dir = env::var("PWD").unwrap_or("/".to_string());
    let mut args: Vec<String> = env::args().collect();
    let mut file_in = "DEF".to_string();
    if args.len() > 1 {
        let file_t = args[1].clone();
        if file_t.chars().nth(0).unwrap() == '/' {
            file_in=file_t;
        }
        else {
            file_in = format!("{}/{}",dir,args.remove(1));
        }
    }
    tui::show_ui(file_in);
}