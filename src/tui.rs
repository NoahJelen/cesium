use std::{fs,env};
use cursive::{
    With,
    Cursive,
    view::Nameable,
    views::{Dialog, ResizedView, TextArea, OnEventView},
};
use cursive::{event::Key, menu::*};

use crate::file_select::*;
use crate::editor::*;

use rust_utils::utils;

pub fn show_ui (file: String) {
    fs::remove_file("/tmp/file_path").unwrap_or(());
    let mut root = cursive::default();

    //path for the theme file
    let theme_path = format!("{}/.config/cesium/",env::var("HOME").expect("Where the hell is your home folder?!"));
    let theme_file = format!("{}/theme.toml",&theme_path);

    //the default theme of the application
    let default_theme = include_str!("../theme.toml");

    //check if the theme file actually exists, load it if it exists
    match root.load_theme_file(&theme_file) {
        Ok(()) =>{},
        Err(_) => {
            //create a new theme file and load defaults if the theme file doesn't exist
            fs::create_dir_all(&theme_path).unwrap_or(());
            fs::write(&theme_file, &default_theme).expect("Unable to write theme file!");
            root.load_toml(default_theme).unwrap();
        }
    }

    root.clear_global_callbacks(cursive::event::Event::CtrlChar('c'));

    root.set_on_pre_event(cursive::event::Event::CtrlChar('c'), quit);

    root.menubar()
    .add_subtree("File",
        MenuTree::new()
        .leaf("New", clear_editor)
        .leaf("Open", load_file)
        .leaf("Save", overwrite)
        .leaf("Save as...",save_new_file)
    )
    .add_subtree("Edit",
        MenuTree::new()
        .leaf("Insert Date", insert_date)
        .leaf("Insert Time", insert_time)
    )
    .add_leaf("Help", show_help)
    .add_leaf("Quit", quit);

    let mut text_t = TextArea::new();
    let mut title = "Cesium".to_string();
    let mut text = TextArea::new();
    if file !="DEF" {
        utils::set_shared_val("file_path", &file);
        text_t.set_content(utils::read_file(utils::get_shared_val("file_path")));
        title = format!("Cesium: {}",utils::get_shared_val("file_path"));
        
        text = text_t;
    }

    let win = Dialog::around(text.with_name("text")).title(title);

    root.add_layer(
        ResizedView::with_full_screen(win.with_name("buffer"))
    );

    root.add_global_callback(Key::F1, |s| s.select_menubar());
    root.set_autohide_menu(false);

    root.run();
}

fn quit(view: &mut Cursive) {
    view.add_layer(
        Dialog::text("Are you sure you want to quit?\nAll unsaved work will be lost!")
        .button("Yes", |s| s.quit())
        .button("No", |s| {
            s.pop_layer();
        })
        .wrap_with(OnEventView::new)
        .on_event(Key::Esc, |s| {
            s.pop_layer();
        })
    );
}

fn show_help(view: &mut Cursive) {
    view.add_layer(
        Dialog::text(HELP_TEXT)
        .dismiss_button("Back")
        .title("Help")
        .wrap_with(OnEventView::new)
        .on_event(Key::Esc, |s| {
            s.pop_layer();
        })
    )
}
const HELP_TEXT: &str = "Cesium: A Notepad-like text editor
Version 0.2.0

Key Shortcuts:
F1: Switch to menu bar
Ctrl-C: Quit the editor (make your file is saved first!)";