use std::{env, fs};
use rust_utils::utils;
use cursive::{
    view::{Nameable, Scrollable},
    views::{Dialog, ResizedView, SelectView, TextArea, EditView, TextView, OnEventView,LinearLayout},
    Cursive, With,
    event::Key
};

//loads a file into the editor buffer
pub fn load_file(view: &mut Cursive) {
    let mut select_pane: SelectView<String> = SelectView::new();
    populate(&mut select_pane, format!("{}/",env::var("HOME").unwrap()),false);
    view.add_layer(ResizedView::with_fixed_height(20,
        Dialog::around(
            LinearLayout::vertical()
            .child(TextView::new(format!("Directory: {}",utils::get_shared_val("file_path"))).with_name("folder"))
            .child(TextView::new("\n"))
            .child(
                select_pane
                    .on_submit(|s, name: &str| {
                        if name != ".." {
                            utils::set_shared_val("file_path", name.to_string());
                        }

                        //change to a child directory
                        if name.chars().last().unwrap() == '/' {
                            s.call_on_name("file_list", change_to_child);
                            s.call_on_name("folder", |text: &mut TextView|text.set_content(format!("Directory: {}",utils::get_shared_val("file_path"))));
                        }
                        //change to the parent directory
                        else if name == ".." {
                            s.call_on_name("file_list", change_to_parent);
                            s.call_on_name("folder", |text: &mut TextView|text.set_content(format!("Directory: {}",utils::get_shared_val("file_path"))));
                        }
                        else {
                            utils::set_shared_val("file_path", name.to_string());
                            s.call_on_name("buffer", |s: &mut Dialog| {
                                s.set_title(format!("Cesium: {}",utils::get_shared_val("file_path")));
                            });
                            s.call_on_name("text", |s: &mut TextArea| {
                                let file = utils::get_shared_val("file_path");
                                s.set_content(fs::read_to_string(file).unwrap())
                            });
                            s.pop_layer();
                        }
                    })
                    .with_name("file_list")
                    .scrollable()
                    .wrap_with(OnEventView::new)
                    .on_event('h', |s| {
                        s.call_on_name("file_list",|list: &mut SelectView|{
                            populate(list, utils::get_shared_val("file_path"), true);
                        });
                    })
                    .on_event('H', |s| {
                        s.call_on_name("file_list",|list: &mut SelectView|{
                            populate(list, utils::get_shared_val("file_path"), false);
                        });
                    })
            )
        )
        .dismiss_button("Cancel")
        .title("Select File:")
        .wrap_with(OnEventView::new)
        .on_event(Key::Esc, |s| {
            s.pop_layer();
        })
        .with_name("file_window")
    ));
}

//saves the editor buffer to a new file
pub fn save_new_file(view: &mut Cursive) {
    view.call_on_name("text", |buf: &mut TextArea|{
        utils::set_shared_val("buffer", buf.get_content());
    });
    let mut select_pane: SelectView<String> = SelectView::new();

    //populate the file selector with files and directories
    populate(&mut select_pane, format!("{}/",env::var("HOME").unwrap()),false);
    
    view.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(TextView::new(format!("Directory: {}",utils::get_shared_val("file_path"))).with_name("folder"))
                .child(TextView::new("\n"))
                .child(ResizedView::with_max_height(20,
                    select_pane
                        //overwrite an existing file
                        .on_submit(|s, name: &str| {
                            if name != ".." {
                                utils::set_shared_val("file_path", name.to_string());
                            }

                            //change to a child directory
                            if name.chars().last().unwrap() == '/' {
                                s.call_on_name("file_list", change_to_child);
                                s.call_on_name("folder", |text: &mut TextView|text.set_content(format!("Directory: {}",utils::get_shared_val("file_path"))));
                            }

                            //change to the parent directory
                            else if name == ".." {
                                s.call_on_name("file_list", change_to_parent);
                                s.call_on_name("folder", |text: &mut TextView|text.set_content(format!("Directory: {}",utils::get_shared_val("file_path"))));
                            }

                            //save the selected file
                            else {
                                s.add_layer(
                                    Dialog::around(TextView::new("Are you sure you want to overwrite this file?"))
                                    .dismiss_button("No")
                                    .button("Yes", |s|{
                                        let file = utils::get_shared_val("file_path");
                                        utils::save_file(file, utils::get_shared_val("buffer"));
                                        s.pop_layer();
                                        s.pop_layer();
                                    })
                                );
                            
                                utils::set_shared_val("file_path", name.to_string());
                            }
                        })

                        //autofill the file name box
                        .on_select(|s, name|{
                            if name.chars().last().unwrap() !='/'{
                                s.call_on_name("filename",|s: &mut EditView|{
                                    let filename = utils::get_filename(name);
                                    s.set_content(filename);
                                });
                            }
                        })
                        .with_name("file_list")
                        .wrap_with(OnEventView::new)
                        .on_event('h', |s| {
                            s.call_on_name("file_list",|list: &mut SelectView|{
                                populate(list, utils::get_shared_val("file_path"), true);
                            });
                        })
                        .on_event('H', |s| {
                            s.call_on_name("file_list",|list: &mut SelectView|{
                                populate(list, utils::get_shared_val("file_path"), false);
                            });
                        })
                        .scrollable(),
                ))
                .child(TextView::new("\n"))
                .child(TextView::new("Name:"))
                .child(EditView::new().with_name("filename"))
        )

        //save to a new file
        .button("Save", |s|{
            let mut exit = false;
            let mut save = true;

            //if the selected item is a directory, change to it
            s.call_on_name("file_list", |list: &mut SelectView|{
                let name = list.selection().unwrap().to_string();
                if name.chars().last().unwrap() == '/' {
                    utils::set_shared_val("file_path", &name);
                    change_to_child(list);
                    save = false;
                }
            });
            s.call_on_name("folder", |text: &mut TextView|text.set_content(format!("Directory: {}",utils::get_shared_val("file_path"))));

            //otherwise save the file
            s.call_on_name("filename",|filename: &mut EditView|{
                let dir = utils::get_shared_val("file_path");
                let name = filename.get_content().to_string();
                let save_name = format!("{}{}",dir,name);
                
                if save{
                    utils::set_shared_val("file_path", &save_name);
                    utils::save_file(save_name, utils::get_shared_val("buffer"));
                    exit = true;
                }
            });
            
            //if a file was saved, exit the menu
            if exit{
                s.call_on_name("buffer", |win: &mut Dialog|{
                    win.set_title(format!("Cesium: {}",utils::get_shared_val("file_path")));
                });
                s.pop_layer();
            }
        })
        .dismiss_button("Cancel")
        .title("Select File:")
        .wrap_with(OnEventView::new)
        .on_event(Key::Esc, |s| {
            s.pop_layer();
        })
        .with_name("file_window")
    );
}

//populates the file selector with files
fn populate(view: &mut SelectView, dir_str: String, show_hidden: bool) {
    //clears the view, if needed
    view.clear();

    //add the parent directory
    if dir_str != "/" {
        view.add_item("<Parent>", "..".to_string());
    }

    //add the child directories and files
    utils::set_shared_val("file_path", &dir_str);
    let dir = utils::get_dir(dir_str, show_hidden);
    for file in dir {
        let name = file.clone();
        let path = format!("{}{}",utils::get_shared_val("file_path"), name.clone());
        view.add_item(name,path);
    }
}

//changes the file selector to a parent directory
fn change_to_parent(view: &mut SelectView) {
    let dir_str = utils::get_parent(utils::get_shared_val("file_path"));
    populate(view, dir_str,false);
}

//changes the file selector view to a child directory
fn change_to_child(view: &mut SelectView){
    populate(view, utils::get_shared_val("file_path"),false);
}